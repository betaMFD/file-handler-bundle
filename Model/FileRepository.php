<?php

namespace BetaMFD\FileHandlerBundle\Model;

use BetaMFD\FileHandlerBundle\Model\FileInterface;

/**
 * FileRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FileRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByFilename($filename, $location = null)
    {
        $qb = $this->createQueryBuilder('f');
        $qb->andWhere('f.filename = :filename');
        $qb->setParameter('filename', $filename);
        if (!empty($location)) {
            $this->andWhereLocation($qb, $location);
        }
        $res = $qb->getQuery()->getResult();
        return $res;
    }

    public function findByLocation($location)
    {
        $qb = $this->createQueryBuilder('f');
        $this->andWhereLocation($qb, $location);
        $res = $qb->getQuery()->getResult();
        return $res;
    }

    protected function andWhereLocation($qb, $location)
    {
        //I don't know, just test for all of the possible endings
        $qb->andWhere('f.location in (:location, :location2, :location3, :location4, :location5, :location6, :location7)');
        $qb->setParameter('location', $location);
        $qb->setParameter('location2', $location . '\\');
        $qb->setParameter('location3', $location . '/');
        $forward = str_replace('\\', '/', $location);
        $back = str_replace('/', '\\', $location);
        $qb->setParameter('location4', $forward . '/');
        $qb->setParameter('location5', $forward . '\\');
        $qb->setParameter('location6', $back . '/');
        $qb->setParameter('location7', $back . '\\');
    }
}
