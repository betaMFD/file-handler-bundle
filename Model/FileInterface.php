<?php

namespace BetaMFD\FileHandlerBundle\Model;

interface FileInterface
{
    public function __construct();

    public function isDeleted();

    public function isImage();

    public function getFilename();

    public function getLocation();

    public function setOriginalFilename($originalFilename);

    public function setContentType($contentType);

    public function setFileSizeB($fileSizeB);

    public function setUploadBy(\BetaMFD\FileHandlerBundle\Model\UserInterface $uploadBy = null);

    public function setLocation($location);

    public function setExtension($extension);

    public function setHash($hash);

    public function getHash();

    public function setFilename($filename);

    public function setUploadDate(\DateTime $uploadDate);

    public function setWidth($width);

    public function setHeight($height);

    public function setPrivatePath($privatePath);
}
