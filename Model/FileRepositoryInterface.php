<?php

namespace BetaMFD\FileHandlerBundle\Model;


interface FileRepositoryInterface
{
    public function findByFilename($filename, $location = null);
}
