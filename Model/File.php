<?php

namespace BetaMFD\FileHandlerBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="file")
 * @ORM\Entity(repositoryClass="FileRepository")
 */
#[ORM\Table(name: "file")]
#[ORM\Entity(repositoryClass: FileRepository::class)]
abstract class File implements FileInterface
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    #[ORM\Column(type: "string", length: 100, nullable: false)]
    protected $filename;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    #[ORM\Column(type: "string", length: 100, nullable: false)]
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    #[ORM\Column(type: "text", nullable: true)]
    protected $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    protected $uploadDate;

    /**
     * @var \BetaMFD\FileHandlerBundle\Model\UserInterface
     * @ORM\ManyToOne(targetEntity="\BetaMFD\FileHandlerBundle\Model\UserInterface")
     * @ORM\JoinColumn(nullable=true)
     */
    #[ORM\ManyToOne(targetEntity: UserInterface::class)]
    #[ORM\JoinColumn(nullable: true)]
    protected $uploadBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    protected $deleteDate;

    /**
     * @ORM\ManyToOne(targetEntity="\BetaMFD\FileHandlerBundle\Model\UserInterface")
     * @ORM\JoinColumn(nullable=true)
     */
    #[ORM\ManyToOne(targetEntity: UserInterface::class)]
    #[ORM\JoinColumn(nullable: true)]
    protected $deleteBy;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    #[ORM\Column(type: "boolean", nullable: false)]
    protected $generated = false;

    /**
     * @var string
     *
     * @ORM\Column(name="file_size", type="decimal", precision=20, scale=4, nullable=true)
     */
    #[ORM\Column(name: "file_size", type: "decimal", precision: 20, scale: 4, nullable: true)]
    protected $fileSize;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4, nullable=false)
     */
    #[ORM\Column(type: "string", length: 4, nullable: false)]
    protected $extension;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    #[ORM\Column(type: "string", length: 100, nullable: false)]
    protected $contentType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    #[ORM\Column(type: "string", length: 100, nullable: false)]
    protected $location;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    #[ORM\Column(type: "boolean", nullable: false)]
    protected $privatePath = true;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    #[ORM\Column(type: "string", length: 100, nullable: true)]
    protected $originalFilename;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    #[ORM\Column(type: "string", length: 64, nullable: true)]
    protected $hash;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    #[ORM\Column(type: "integer", nullable: true)]
    protected $height;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    #[ORM\Column(type: "integer", nullable: true)]
    protected $width;

    public function __construct()
    {
        $this->setUploadDate(new \DateTime);
    }

    public function getPath()
    {
        return $this->location . $this->filename;
    }

    public function isDeleted()
    {
        return !empty($this->deleteDate);
    }

    public function isImage()
    {
        return $this->contentType == 'image/jpeg'
            or $this->contentType == 'image/jpg'
            or $this->contentType == 'image/png'
            or $this->contentType == 'image/gif';
    }

    public function isPdf()
    {
        return $this->contentType == 'application/pdf';
    }

    public function isPrivatePath()
    {
        return $this->privatePath;
    }

    public function isPublicPath()
    {
        return !$this->privatePath;
    }

    public function getAlt()
    {
        if (!empty($this->title)) {
            return $this->title;
        }
        if (!empty($this->description)) {
            return $this->description;
        }
    }

    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set the value of Filename
     *
     * @param string filename
     *
     * @return self
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        if (empty($this->extension)) {
            $ext = substr($filename, -3);
            if ($ext == 'peg') {
                $this->setExtension('jpeg');
            }
            if ($ext == 'lsx') {
                $this->setExtension('xlsx');
            } else {
                $this->setExtension($ext);
            }
        }

        return $this;
    }

    /**
     * Get the value of Title
     *
     * @return string
     */
    public function getTitle()
    {
        if (empty($this->title)) {
            return $this->filename;
        }
        return $this->title;
    }

    /**
     * Set the value of Title
     *
     * @param string title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Upload Date
     *
     * @return \DateTime
     */
    public function getUploadDate()
    {
        return $this->uploadDate;
    }

    /**
     * Set the value of Upload Date
     *
     * @param \DateTime uploadDate
     *
     * @return self
     */
    public function setUploadDate(\DateTime $uploadDate)
    {
        $this->uploadDate = $uploadDate;

        return $this;
    }

    /**
     * Get the value of Upload By
     *
     * @return \BetaMFD\FileHandlerBundle\Model\UserInterface
     */
    public function getUploadBy()
    {
        return $this->uploadBy;
    }

    /**
     * Set the value of Upload By
     *
     * @param \BetaMFD\FileHandlerBundle\Model\UserInterface uploadBy
     *
     * @return self
     */
    public function setUploadBy(\BetaMFD\FileHandlerBundle\Model\UserInterface $uploadBy = null)
    {
        $this->uploadBy = $uploadBy;

        return $this;
    }

    /**
     * Get the value of Delete Date
     *
     * @return \DateTime
     */
    public function getDeleteDate()
    {
        return $this->deleteDate;
    }

    /**
     * Set the value of Delete Date
     *
     * @param \DateTime deleteDate
     *
     * @return self
     */
    public function setDeleteDate(\DateTime $deleteDate = null)
    {
        $this->deleteDate = $deleteDate;

        return $this;
    }

    /**
     * Get the value of Delete By
     *
     * @return mixed
     */
    public function getDeleteBy()
    {
        return $this->deleteBy;
    }

    /**
     * Set the value of Delete By
     *
     * @param mixed deleteBy
     *
     * @return self
     */
    public function setDeleteBy(\BetaMFD\FileHandlerBundle\Model\UserInterface $deleteBy = null)
    {
        $this->deleteBy = $deleteBy;

        return $this;
    }

    /**
     * Get the value of Generated
     *
     * @return boolean
     */
    public function getGenerated()
    {
        return $this->generated;
    }

    /**
     * Set the value of Generated
     *
     * @param boolean generated
     *
     * @return self
     */
    public function setGenerated($generated)
    {
        $this->generated = $generated;

        return $this;
    }

    /**
     * Get the value of File Size
     *
     * @return string
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * Set the value of File Size
     *
     * @param string fileSize
     *
     * @return self
     */
    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;

        return $this;
    }
    /**
     * Get the value of File Size
     *
     * @return string
     */
    public function getFileSizeMB()
    {
        return $this->fileSize / 1024 / 1024;
    }

    /**
     * Set the value of File Size
     *
     * @param string fileSizeMB
     *
     * @return self
     */
    public function setFileSizeMB($fileSizeMB)
    {
        $this->fileSize = $fileSizeMB * 1024 * 1024;

        return $this;
    }

    public function setFileSizeKB($fileSizeKB)
    {
        $this->fileSize = $fileSizeKB * 1024;

        return $this;
    }

    public function setFileSizeB($fileSizeB)
    {
        $this->fileSize = $fileSizeB;

        return $this;
    }

    /**
     * Get the value of Extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set the value of Extension
     *
     * @param string extension
     *
     * @return self
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
        if (empty($this->getContentType())) {
            switch (strtolower($extension)) {
                case 'pdf':
                $this->setContentType('application/pdf');
                break;
                case 'jpg':
                case 'jpeg':
                $this->setContentType('image/jpeg');
                break;
                case 'csv':
                case 'txt':
                $this->setContentType('text/csv');
                break;
                case 'xls':
                $this->setContentType('application/vnd.ms-excel');
                break;
                case 'xlsx':
                $this->setContentType('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                break;
            }
        }

        return $this;
    }

    /**
     * Get the value of Content Type
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * Set the value of Content Type
     *
     * @param string contentType
     *
     * @return self
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Get the value of Location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set the value of Location
     *
     * @param string location
     *
     * @return self
     */
    public function setLocation($location)
    {
        //trim whitespace
        $location = trim($location);
        //force all slashes to be forward
        $location = str_replace('\\', '/', $location);
        //remove possible double slashes
        $end = substr($location, -2);
        if ($end == '//') {
            //if string ends in //, remove one character from the end of it
            $location = substr($location, 0, (strlen($location) - 1));
        }
        $this->location = $location;

        return $this;
    }

    /**
     * Get the value of Original Filename
     *
     * @return string
     */
    public function getOriginalFilename()
    {
        return $this->originalFilename;
    }

    /**
     * Set the value of Original Filename
     *
     * @param string originalFilename
     *
     * @return self
     */
    public function setOriginalFilename($originalFilename)
    {
        $this->originalFilename = $originalFilename;

        return $this;
    }

    /**
     * Get the value of Hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set the value of Hash
     *
     * @param string hash
     *
     * @return self
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get the value of Height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Math out the value of Height when you want to set the Width something
     * @param integer $new_width
     *
     * @return integer
     */
    public function getAdjustedHeight($new_width)
    {
        if (empty($this->height)) {
            return $this->height;
        }
        return round($new_width * $this->height / $this->width, 0);
    }

    /**
     * Set the value of Height
     *
     * @param integer $height
     *
     * @return self
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get the value of Width
     *
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Math out the value of Width when you want to set the height something
     * @param integer $new_width
     *
     * @return integer
     */
    public function getAdjustedWidth($new_height)
    {
        if (empty($this->width)) {
            return $this->width;
        }
        return round($new_height * $this->width / $this->height, 0);
    }

    /**
     * Set the value of Width
     *
     * @param integer width
     *
     * @return self
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Set the value of Private Path
     *
     * @param boolean $privatePath
     *
     * @return self
     */
    public function setPrivatePath($privatePath)
    {
        $this->privatePath = $privatePath;

        return $this;
    }
}
