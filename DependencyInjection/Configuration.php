<?php

namespace BetaMFD\FileHandlerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        // Pass the root node name to the TreeBuilder constructor
        $treeBuilder = new TreeBuilder('beta_mfd_file_handler');

        // Use getRootNode() instead of root() to define the root node
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('private_upload_location')
                ->defaultValue('%kernel.project_dir%/var/uploads/')
            ->end()
                ->scalarNode('public_upload_location')
                ->defaultValue('%kernel.project_dir%/web/uploads/')
            ->end()
                ->scalarNode('file_entity')
                ->defaultValue('App/Entity/File')
            ->end()
            ->end();

        return $treeBuilder;
    }
}
